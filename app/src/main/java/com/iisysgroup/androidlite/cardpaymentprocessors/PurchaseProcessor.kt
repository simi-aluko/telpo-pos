package com.iisysgroup.androidlite.cardpaymentprocessors

import android.os.Bundle
import android.util.Log
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.payments_menu.BasePaymentActivity
import com.iisysgroup.androidlite.payments_menu.handlers.Purchase
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.newland.NewLandEmvControllerListener
import com.iisysgroup.newland.NewlandDevice
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.TransactionResult
import com.iisysgroup.poslib.utils.AccountType
import com.iisysgroup.poslib.utils.InputData
import com.itex.richard.payviceconnect.wrapper.PayviceServices
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync

class PurchaseProcessor : BaseCardPaymentProcessor(){
    //Receives amount, additional amount, account type,
    //Returns value to calling library or class


    private val mAmount by lazy {
        intent.getLongExtra(BasePaymentActivity.TRANSACTION_AMOUNT, 0L)
    }

    private val mAdditionalAmount by lazy {
        intent.getLongExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)
    }

    private val mAccountType by lazy {
        intent.getSerializableExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE) as AccountType
    }


    override fun initializeDefaultUI(): DefaultUIModel {
        return DefaultUIModel(transactionTitle = "Purchase", amount = mAmount)
    }

    private val mDb by lazy {
        (application as App).db
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val inputData = InputData(mAmount, mAdditionalAmount, mAccountType)
        val purchase = Purchase(this, mDb, inputData, mHostInteractor, mConnectionData, mEmvInteractor)

        purchase.getTransactionResult2()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->
                    Log.d("SimiPurchaseProcessor", result.toString())
                    error?.let {
                        it.printStackTrace()
                        Log.d("Simi", it.toString())
                    }

                    result?.let {
                        Log.d("OkHTransactionResult",  it.toString())
                        mEmvInteractor.processOnlineResponse(it.responseCode, it.issuerAuthData91, it.issuerScript71, it.issuerScript72)

                        DbManager(application).saveTransactionData(it)
                        setTransactionRrn(it.RRN)

                    }
                }
    }
}
