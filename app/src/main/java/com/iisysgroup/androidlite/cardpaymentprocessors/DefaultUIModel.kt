package com.iisysgroup.androidlite.cardpaymentprocessors

data class DefaultUIModel(val transactionTitle : String, val amount : Long, val additionalAmount : Long = 0)