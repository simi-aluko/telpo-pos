package com.iisysgroup.androidlite.cardpaymentprocessors

import android.app.Activity
import android.arch.lifecycle.LiveDataReactiveStreams
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.newland.NewLandEmvControllerListener
import com.iisysgroup.newland.NewlandDevice
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.deviceinterface.interactors.EmvInteractor
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.TransactionResult
import com.telpo.moduled.Telpo900Device
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.contentView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

abstract class BaseCardPaymentProcessor : AppCompatActivity() {


    //todo this should not be compulsory - users can decide to use default UI
    abstract fun initializeDefaultUI() : DefaultUIModel


    private lateinit var uiManager : DefaultUIManager

    lateinit var mTelpo900Device: Telpo900Device
//    lateinit var mAmpDevice : NewlandDevice
    lateinit var mEmvInteractor : EmvInteractor
    lateinit var mConnectionData: ConnectionData
    lateinit var mHostInteractor: HostInteractor

    lateinit var mUIModel : DefaultUIModel

//    lateinit var newland : NewlandDevice

    private var mRrn : String = ""

    private var keyholder : String = ""

    lateinit var keyHolder : KeyHolder

    private val mDb by lazy {
        (application as App).db
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_process)

//        doAsync {
//           keyHolder =  mDb.keyHolderDao.get()
//            Log.d("masterKey", keyHolder.masterKey)
//            Log.d("pinKey", keyHolder.pinKey)
//            val mainkey = SecureStorage.retrieve("mainkey", "")
//            val pinkey = SecureStorage.retrieve("pinkey", "")
//            //newland.loadKeys(mainkey, pinkey)
//            NewlandDevice(this@BaseCardPaymentProcessor, mainkey, pinkey)
//        }


        //(application as App).db.configDataDao.get()

        mUIModel = initializeDefaultUI()
        setUpDeviceInteractor()
        setDeviceStatusObserver()

        setUpHostInteractor()
        setUpConnectionData()

    }

    private fun setUpDeviceInteractor() {

//        mAmpDevice = NewlandDevice(this)
        mTelpo900Device = Telpo900Device(this)
        mEmvInteractor = EmvInteractor.getInstance(mTelpo900Device)

    }

    private fun setUpHostInteractor() {
        mHostInteractor = (application as App).hostInteractor
    }

    private fun setUpConnectionData() {
        val terminalId = SharedPreferenceUtils.getTerminalId(this)
        val ipAddress = SharedPreferenceUtils.getIpAddress(this)
        val ipPort = SharedPreferenceUtils.getPort(this)
        val isSSL = SharedPreferenceUtils.isSsl(this)

        when {
            terminalId.isNullOrEmpty() -> {
                toast("Enter valid terminal Id")
                return
            }
            ipAddress.isNullOrEmpty() -> toast("Enter valid IP Address")
            ipPort.toString().isNullOrEmpty() -> toast("Enter valid IP Port")
        }

        mConnectionData = ConnectionData(terminalId, ipAddress, ipPort.toInt(), isSSL)

    }

    fun setTransactionRrn(rrn : String){
        this.mRrn = rrn
    }

    open fun setDeviceStatusObserver(){

        uiManager = DefaultUIManager(contentView!!, mUIModel)

        toast("simi2")
        LiveDataReactiveStreams.fromPublisher(mEmvInteractor.observeStatus()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()))
                .observe({ lifecycle }){

                    toast("simi3")

                    it?.let{
                        val intent = Intent()
                        intent.putExtra("state", it.state)

                        when {
                            it.state == DeviceState.DECLINED -> {
                                mHostInteractor.rollBackTransaction().subscribe { transactionResult ->
                                    Log.d("OkHRollback", transactionResult.toString())
                                    intent.putExtra("rrn", transactionResult.RRN)
                                    setResult(Activity.RESULT_OK, intent)
                                    finish()
                                }

                            }
                            it.state == DeviceState.FAILED -> {
                                toast("Failed")
                                intent.putExtra("rrn", mRrn)

                                setResult(Activity.RESULT_OK, intent)
                                finish()
                            }

                            it.state == DeviceState.AWAITING_ONLINE_RESPONSE -> {
                                Log.d("OkH", "Awaiting online response")
                            }

                            it.state == DeviceState.APPROVED -> {
                                intent.putExtra("rrn", mRrn)
                                setResult(Activity.RESULT_OK, intent)
                                finish()
                            }
                        }

                        Log.d("UI_STATE", it.state.toString())
                        uiManager.setState(it.state)
                    }
                }


    }
}