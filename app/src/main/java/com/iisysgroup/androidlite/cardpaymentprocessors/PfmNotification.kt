package com.iisysgroup.androidlite.cardpaymentprocessors

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import com.iisysgroup.androidlite.pfm.PfmState
import com.iisysgroup.poslib.host.entities.TransactionResult
import java.util.*
import android.os.BatteryManager
import android.content.Context.BATTERY_SERVICE
import android.net.NetworkInfo
import android.net.ConnectivityManager
import android.util.Log
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.pfm.NotificationModel
import com.iisysgroup.androidlite.pfm.PfmJournal
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody


class PfmNotification() {


    fun sendNotification(transactionResult: TransactionResult, context: Context){
        val bm = context.getSystemService(BATTERY_SERVICE) as BatteryManager
        val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        var cstate = "";


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(bm.isCharging){
                cstate = "isCharging"
            }else{
                cstate = "NotCharging"
            }
        } else {

                cstate = "NotCharging"
        }
        var pfmstate = PfmState(Build.SERIAL, Date().toGMTString(),batLevel,cstate,"nil",transactionResult.terminalID,getNetworkType(context),
                "cid:,lac:,mcc:,mnc:,ss","","ME30S","Newland MPOS - " + Build.MANUFACTURER,"true",context.getPackageManager()
                .getPackageInfo(context.getPackageName(), 0).versionCode.toString(),Date(transactionResult.longDateTime).toGMTString(),"OK","");

        var pfmJournal = PfmJournal(transactionResult.STAN,transactionResult.PAN,transactionResult.cardHolderName,transactionResult.cardExpiry,transactionResult.RRN,transactionResult.authID,transactionResult.amount.toInt(),Date(transactionResult.longDateTime).toGMTString(),
                "","",transactionResult.responseCode,"true",transactionResult.transactionStatusReason,"",transactionResult.STAN, transactionResult.RRN,transactionResult.authID,transactionResult.merchantID,"","","",
                "Card","","");

        val notificationModel = NotificationModel(pfmstate,pfmJournal)

        launch(CommonPool){
            val client = OkHttpClient()
            Log.i("okh", "Background Pfm call started")

            val mediaType = MediaType.parse("application/json")
            Log.i("okh", notificationModel.build(SharedPreferenceUtils.getNotificationUser(context)))
            val body = RequestBody.create(mediaType, notificationModel.build(SharedPreferenceUtils.getNotificationUser(context)))
            val request = Request.Builder()
                    .url("https://pfm.payvice.com/api/tms/iisys/auth2")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("authorization", "IISYS 74f230cc6cc96f7672aeb1f1745ccaec56de6e61f1d2ef2122441040ec58d044")
                    .addHeader("iisysgroup", "21155ded2430abf93108bef7a62cf2cca1bcf3c3ea8a75e6527a53409be495d0")
                    .build()

            val response = client.newCall(request).execute()
            Log.i("okh", "Background call completed with res code of " + response.code() + " and body of " + response.body()!!.string());
        }
    }

    fun sendNotification(transactionResult: TransactionResult, context: Context, vasPorduct: String,vasCategory : String){
        val bm = context.getSystemService(BATTERY_SERVICE) as BatteryManager
        val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        var cstate = "";


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(bm.isCharging){
                cstate = "isCharging"
            }else{
                cstate = "NotCharging"
            }
        } else {

            cstate = "NotCharging"
        }
        var pfmstate = PfmState(Build.SERIAL, Date().toGMTString(),batLevel,cstate,"nil",transactionResult.terminalID,getNetworkType(context),
                "cid:,lac:,mcc:,mnc:,ss","","ME30S","Newland MPOS - " + Build.MANUFACTURER,"true",context.getPackageManager()
                .getPackageInfo(context.getPackageName(), 0).versionCode.toString(),Date(transactionResult.longDateTime).toGMTString(),"OK","");

        var pfmJournal = PfmJournal(transactionResult.STAN,transactionResult.PAN,transactionResult.cardHolderName,transactionResult.cardExpiry,transactionResult.RRN,transactionResult.authID,transactionResult.amount.toInt(),Date(transactionResult.longDateTime).toGMTString(),
                "","",transactionResult.responseCode,"true",transactionResult.transactionStatusReason,"",transactionResult.STAN, transactionResult.RRN,transactionResult.authID,transactionResult.merchantID,vasCategory,vasPorduct,"",
                "Card","","");

        val notificationModel = NotificationModel(pfmstate,pfmJournal)

        launch(CommonPool){
            val client = OkHttpClient()
            Log.i("okh", "Background Pfm call started")

            val mediaType = MediaType.parse("application/json")
            Log.i("okh", notificationModel.build(SharedPreferenceUtils.getNotificationUser(context)))
            val body = RequestBody.create(mediaType, notificationModel.build(SharedPreferenceUtils.getNotificationUser(context)))
            val request = Request.Builder()
                    .url("https://pfm.payvice.com/api/tms/iisys/auth2")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("authorization", "IISYS 74f230cc6cc96f7672aeb1f1745ccaec56de6e61f1d2ef2122441040ec58d044")
                    .addHeader("iisysgroup", "21155ded2430abf93108bef7a62cf2cca1bcf3c3ea8a75e6527a53409be495d0")
                    .build()

            val response = client.newCall(request).execute()
            Log.i("okh", "Background call completed with res code of " + response.code() + " and body of " + response.body()!!.string());
        }
    }

    fun sendNotification(vasPorduct: String,vasCategory : String, amount : Int , context: Context, rescode : String, mid : String){
        val bm = context.getSystemService(BATTERY_SERVICE) as BatteryManager
        val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        var cstate = "";


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(bm.isCharging){
                cstate = "isCharging"
            }else{
                cstate = "NotCharging"
            }
        } else {

            cstate = "NotCharging"
        }
        var pfmstate = PfmState(Build.SERIAL, Date().toGMTString(),batLevel,cstate,"nil",SharedPreferenceUtils.getTerminalId(context),getNetworkType(context),
                "cid:,lac:,mcc:,mnc:,ss","","ME30S","Newland MPOS - " + Build.MANUFACTURER,"true",context.getPackageManager()
                .getPackageInfo(context.getPackageName(), 0).versionCode.toString(),Date().toGMTString(),"OK","");

        var pfmJournal = PfmJournal("","","","","","",amount,Date().toGMTString(),
                "","",rescode,"true","","","", "","",mid,vasCategory,vasPorduct,"",
                "Cash","","");

        val notificationModel = NotificationModel(pfmstate,pfmJournal)

        launch(CommonPool){
            val client = OkHttpClient()
            Log.i("okh", "Background Pfm call started")

            val mediaType = MediaType.parse("application/json")
            Log.i("okh", notificationModel.build(SharedPreferenceUtils.getNotificationUser(context)))
            val body = RequestBody.create(mediaType, notificationModel.build(SharedPreferenceUtils.getNotificationUser(context)))
            val request = Request.Builder()
                    .url("https://pfm.payvice.com/api/tms/iisys/auth2")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("authorization", "IISYS 74f230cc6cc96f7672aeb1f1745ccaec56de6e61f1d2ef2122441040ec58d044")
                    .addHeader("iisysgroup", "21155ded2430abf93108bef7a62cf2cca1bcf3c3ea8a75e6527a53409be495d0")
                    .build()

            val response = client.newCall(request).execute()
            Log.i("okh", "Background call completed with res code of " + response.code() + " and body of " + response.body()!!.string());
        }
    }

    fun getNetworkType(context: Context): String {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = cm.activeNetworkInfo
        return info.typeName
    }
}