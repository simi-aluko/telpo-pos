package com.iisysgroup.androidlite.payments_menu.transfer

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.PrintActivity
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.cardpaymentprocessors.PurchaseProcessor
import com.iisysgroup.androidlite.cardpaymentprocessors.VasPurchaseProcessor
import com.iisysgroup.androidlite.login.Helper
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.models.LookupSuccessModel
import com.iisysgroup.androidlite.models.ReceiptModel
import com.iisysgroup.androidlite.payments_menu.BasePaymentActivity
import com.iisysgroup.androidlite.payments_menu.Services.TransferService
import com.iisysgroup.androidlite.payments_menu.models.AccountLookUpDetails
import com.iisysgroup.androidlite.payments_menu.models.TransactionDetails
import com.iisysgroup.androidlite.payments_menu.models.TransferDetails
import com.iisysgroup.androidlite.payments_menu.utils.HashUtils
import com.iisysgroup.androidlite.utils.PinAlertUtils
import com.iisysgroup.androidlite.utils.PrintUtils
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.androidlite.utils.TimeUtils
import com.iisysgroup.newland.NewlandDevice
import com.iisysgroup.payvice.securestorage.SecureStorageUtils
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.deviceinterface.interactors.PrinterInteractor
import com.iisysgroup.poslib.utils.AccountType
//import com.telpo.moduled.Telpo900Device
import kotlinx.android.synthetic.main.activity_transfer_amount_entry.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import me.eugeniomarletti.kotlin.metadata.shadow.util.capitalizeDecapitalize.capitalizeFirstWord
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast
import java.math.BigDecimal
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.URLEncoder
import java.text.DecimalFormat
import java.util.*

class TransferAmountEntry : AppCompatActivity(), View.OnClickListener  {

    enum class TRANSACTION_TYPE {TRANSFER, WITHDRAWAL, DEPOSIT}

    private lateinit var mConvenienceFee : String
    private lateinit var mRrn : String

    private lateinit var mEncryptedPin : String

    private val mTerminalId by lazy {
        SharedPreferenceUtils.getTerminalId(this)
    }

    private val printerInteractor by lazy {
        PrinterInteractor.getInstance(NewlandDevice(this))
    }

    private val mTransactionType by lazy {
        intent.getSerializableExtra(TransferBankSelection.TRANSACTION_TYPE) as TRANSACTION_TYPE
    }

    private val progressDialog by lazy {
        indeterminateProgressDialog(message = "Processing")
    }

    private val mBankName by lazy {
        intent.getStringExtra(TransferBankSelection.BANK_NAME)
    }

    private val mBankCode by lazy {
        intent.getStringExtra(TransferBankSelection.BANK_CODE)
    }

    private val mAccountNumber by lazy {
        intent.getStringExtra(TransferBankSelection.ACCOUNT_NUMBER)
    }

    private val mWalletUsername by lazy {
        SharedPreferenceUtils.getPayviceUsername(this)
    }

    private val mWalletId by lazy {
        SharedPreferenceUtils.getPayviceWalletId(this)
    }

    private val mWalletPassword by lazy {
        SharedPreferenceUtils.getPayvicePassword(this)
    }


    private lateinit var mAccountName : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_amount_entry)

        initializeAmountEntryElements()
    }

    override fun onClick(view: View) {

        val displayStr = StringBuilder(txtAmount.text.toString())
        when (view.id) {
            R.id.btn1 -> fixAppend(displayStr, "1")
            R.id.btn2 -> fixAppend(displayStr, "2")
            R.id.btn3 -> fixAppend(displayStr, "3")
            R.id.btn4 -> fixAppend(displayStr, "4")
            R.id.btn5 -> fixAppend(displayStr, "5")
            R.id.btn6 -> fixAppend(displayStr, "6")
            R.id.btn7 -> fixAppend(displayStr, "7")
            R.id.btn8 -> fixAppend(displayStr, "8")
            R.id.btn9 -> fixAppend(displayStr, "9")
            R.id.btn0 -> fixAppend(displayStr, "0")
            R.id.btn00 -> fixAppend(displayStr, "00")
            R.id.btnenter -> onEnterPressed()
            R.id.btnclr -> {
                if (displayStr.length == 1) {
                    displayStr.deleteCharAt(0)
                    fixDelete(displayStr)
                } else if (displayStr.length > 1) {
                    val index = displayStr.length - 1
                    displayStr.deleteCharAt(index)
                    fixDelete(displayStr)
                }
            }
            R.id.btncancel -> onBackPressed()

        }
    }

    private fun onEnterPressed() {
        if (txtAmount.text.toString().isNotEmpty()){
            verifyAccountDetails()
        } else {
            txtAmount.error = "Enter valid amount"
        }

    }

    private fun payWithWallet(response: LookupSuccessModel) {
        progressDialog.show()
        val amount = (txtAmount.text.toString().toDouble() * 100).toInt()
        Log.d("amount ", amount.toString())

        val action = when (mTransactionType){
            TRANSACTION_TYPE.TRANSFER -> "transfer"
            TRANSACTION_TYPE.DEPOSIT -> "deposit"
            TRANSACTION_TYPE.WITHDRAWAL -> "withdrawal"
        }


        val view = View.inflate(this, R.layout.activity_enter_pin, null)
        val encryptedPassword = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")

        PinAlertUtils.getPin(this, view){
            val encryptedPin = SecureStorageUtils.hashIt(it!!, encryptedPassword)

            launch(CommonPool){
                val transferDetails = TransactionDetails(action = action, beneficiary = mAccountNumber, vendorBankCode =mBankCode, walletID = mWalletId, username = mWalletUsername, password = mWalletPassword, amount = amount.toString(), method = "cash", pin = encryptedPin!!)
                val gson = Gson()
                val jsonPayload = gson.toJson(transferDetails)


                val base64encoded = String(org.apache.commons.codec.binary.Base64.encodeBase64(jsonPayload.toByteArray()))
                val encoded = URLEncoder.encode(base64encoded, "UTF-8")
                val nonce = "${Calendar.getInstance().timeInMillis}$mWalletId$encoded"

                val encryptedStuff = "${nonce}BisiG03sToSk00lMak3sM0nEyAnDCanN0wS3ndMon3yViaTh1sAP1$encoded"
                val signature = HashUtils.sha512(encryptedStuff)

                try {
                    val response =  TransferService.create().transfer(transferDetails, nonce = nonce, signature = signature).await()

                    val isApproved = response.status == 1

                    val status = if (isApproved) {
                        "Approved"
                    } else {
                        "Declined"
                    }

                    val transferDetails = TransferDetails(mTransactionType, isApproved, mAccountName, transferDetails.amount, mConvenienceFee, mBankName, mTerminalId)

                    val amounts = transferDetails.amount.toFloat()/100
                    val fees = transferDetails.fee.toFloat()/100
                    Log.d("amounts", amounts.toString())
                        val map = hashMapOf<String, String>(
                                "Transaction approved" to isApproved.toString().capitalize(),
                                "Terminal ID" to transferDetails.terminalId,
                                "Bank Name" to transferDetails.bankName,
                                "Beneficiary" to transferDetails.beneficiary,

                                //"Amount" to ( transferDetails.amount.toDouble().toInt() /100).toString(),
                                "Amount" to ("₦"+(amounts).toString()),
                                "Fee" to ("₦"+fees).toString()
                        )

                        val receiptModel = ReceiptModel("", "Transfer using wallet", status, map, (amounts).toString(), "")

                        val intent = Intent(this@TransferAmountEntry, PrintActivity::class.java)
                        intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_MODEL_KEY, receiptModel)
                        intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.NOT_INCLUDED)
                        startActivity(intent)
                        finish()



                } catch (e : ConnectException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Connection not established. Please try again"
                            okButton {  }
                        }.show()
                    }
                } catch (e : SocketTimeoutException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Connection taking too long to be established. Please try again"
                            okButton { onBackPressed() }
                        }.show()
                    }

                } catch (e : retrofit2.HttpException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Error from server. Please try again"
                            okButton {  }
                        }.show()
                    }
                } catch (e : com.google.gson.JsonSyntaxException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Error from server. Please try again"
                            okButton {  }
                        }.show()
                    }
                }
            }
        }
    }

    private fun completeCardPayment(){
        progressDialog.show()
        val amount = txtAmount.text.toString().toDouble() * 100

        val action = when (mTransactionType){
            TRANSACTION_TYPE.TRANSFER -> "transfer"
            TRANSACTION_TYPE.DEPOSIT -> "deposit"
            TRANSACTION_TYPE.WITHDRAWAL -> "withdrawal"
        }


            val transferDetails = TransactionDetails(action = action, beneficiary = mAccountNumber, vendorBankCode =mBankCode, walletID = mWalletId, username = mWalletUsername, password = mWalletPassword, amount = amount.toString(), method = "card", pin = mEncryptedPin)



            launch(CommonPool){
                val gson = Gson()
                val jsonPayload = gson.toJson(transferDetails)
                val base64encoded = String(org.apache.commons.codec.binary.Base64.encodeBase64(jsonPayload.toByteArray()))
                val encoded = URLEncoder.encode(base64encoded, "UTF-8")
                val nonce = "${Calendar.getInstance().timeInMillis}$mWalletId$encoded"
                val encryptedStuff = "${nonce}BisiG03sToSk00lMak3sM0nEyAnDCanN0wS3ndMon3yViaTh1sAP1$encoded"
                val signature = HashUtils.sha512(encryptedStuff)


                try {
                    val response =  TransferService.create().transfer(transferDetails, nonce = nonce, signature = signature).await()

                    Log.i("okh", response.toString())


                    val isApproved = response.status == 1
                    (application as App).db.transactionResultDao.get(mRrn).observe({lifecycle}){
                        val transferDetails = TransferDetails(mTransactionType, isApproved, mAccountName, transferDetails.amount, mConvenienceFee, mBankName, mTerminalId)

                        it?.let {
                            transactionResult ->

                            val map = hashMapOf<String, String>(
                                    "MID" to transactionResult.merchantID,
                                    "RRN" to transactionResult.RRN,
                                    "Transaction approved" to isApproved.toString(),
                                    "Terminal ID" to transferDetails.terminalId,
                                    "Card Holder" to transactionResult.cardHolderName,
                                    "Card Expiry" to transactionResult.cardExpiry,
                                    "PAN" to transactionResult.PAN,
                                    "STAN" to transactionResult.STAN,
                                    "Auth ID" to transactionResult.authID,
                                    "Bank Name" to transferDetails.bankName,
                                    "Beneficiary" to transferDetails.beneficiary,
                                    "Amount" to transferDetails.amount,
                                    "Fee" to transferDetails.fee
                            )

                            val date = TimeUtils.convertLongToString(transactionResult.longDateTime)
                            val receiptModel = ReceiptModel(date, "Transfer", transactionResult.transactionStatus, map, (transferDetails.amount.toFloat()/100).toString(), transactionResult.transactionStatusReason)

                            val intent = Intent(this@TransferAmountEntry, PrintActivity::class.java)
                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_MODEL_KEY, receiptModel)
                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.NOT_INCLUDED)
                            //finish()
                            startActivity(intent)
                            finish()
                        }

                    }

                }
                catch (e : SocketTimeoutException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Connection taking too long to be established. Please try again"
                            okButton { onBackPressed() }
                        }.show()
                    }

                }
                catch (e : ConnectException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Connection not established. Please try again"
                            okButton {  }
                        }.show()
                    }

                }
                catch (e : retrofit2.HttpException) {
                    launch(UI) {
                        launch(UI) {
                            progressDialog.dismiss()
                            alert {
                                title = "Error"
                                message = "Error from server. Please try again"
                                okButton { }
                            }.show()
                        }
                    }
                }
                catch (e : com.google.gson.JsonSyntaxException){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = "Error from server. Please try again"
                            okButton {  }
                        }.show()
                    }
                } catch (e : Exception){
                launch(UI){
                    progressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton {  }
                    }.show()
                }
            }

            }
        }


    private fun payWithCard(response: LookupSuccessModel) {
        val view = View.inflate(this, R.layout.activity_enter_pin, null)
        val encryptedPassword = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")

        PinAlertUtils.getPin(this, view){
            mEncryptedPin = SecureStorageUtils.hashIt(it!!, encryptedPassword)!!


            val intent = Intent(this, VasPurchaseProcessor::class.java)
            intent.putExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE, AccountType.DEFAULT_UNSPECIFIED)


            //times 100 because of the conversion to kobo
            val amount = (txtAmount.text.toString().toFloat() * 100) + response.convenienceFee

            intent.putExtra(BasePaymentActivity.TRANSACTION_AMOUNT,  amount.toLong())
            intent.putExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)
            startActivityForResult(intent, 9090)
        }

    }

    private fun initializeAmountEntryElements() {
        findViewById<View>(R.id.btn1).setOnClickListener(this)
        findViewById<View>(R.id.btn2).setOnClickListener(this)
        findViewById<View>(R.id.btn3).setOnClickListener(this)
        findViewById<View>(R.id.btn4).setOnClickListener(this)
        findViewById<View>(R.id.btn5).setOnClickListener(this)
        findViewById<View>(R.id.btn6).setOnClickListener(this)
        findViewById<View>(R.id.btn7).setOnClickListener(this)
        findViewById<View>(R.id.btn8).setOnClickListener(this)
        findViewById<View>(R.id.btn9).setOnClickListener(this)
        findViewById<View>(R.id.btn0).setOnClickListener(this)
        findViewById<View>(R.id.btn00).setOnClickListener(this)
        findViewById<View>(R.id.btnclr).setOnClickListener(this)
        findViewById<View>(R.id.btnenter).setOnClickListener(this)
        findViewById<View>(R.id.btncancel).setOnClickListener(this)
    }

    private fun fixAppend(displayStr : StringBuilder, digit : String) {
        if(displayStr.length <= 11)
        {
            displayStr.append(digit)
            var newAmount = displayStr.toString().toDouble()
            // fix new input
            newAmount *= 10.00
            if("00" == digit) newAmount *= 10.00


            val updatedAmount = DecimalFormat("0.00").format(newAmount)
            txtAmount.text = updatedAmount.toString()
        }
    }

    private fun fixDelete(displayStr: StringBuilder) {
        var bd = BigDecimal(displayStr.toString())
        bd = bd.movePointLeft(1)

        txtAmount.text = bd.toString()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 9090){
            if (resultCode == Activity.RESULT_OK) {

                    val state = data?.getSerializableExtra("state") as DeviceState
                    mRrn = data.getStringExtra("rrn")

                    when (state){
                        DeviceState.DECLINED, DeviceState.FAILED -> {
                          alert {
                                title = "Transaction Result"
                                message = "Transaction declined. Please try again later"
                                positiveButton(buttonText = "Print"){
                                    (application as App).db.transactionResultDao.get(mRrn).observe({lifecycle}){

                                        it?.let {
                                            transactionResult ->

                                            val map = hashMapOf<String, String>(
                                                    "MID" to transactionResult.merchantID,
                                                    "RRN" to transactionResult.RRN,
                                                    "Transaction approved" to "False",
                                                    "Terminal ID" to mTerminalId,
                                                    "Card Holder" to transactionResult.cardHolderName,
                                                    "Card Expiry" to transactionResult.cardExpiry,
                                                    "PAN" to transactionResult.PAN,
                                                    "STAN" to transactionResult.STAN,
                                                    "Auth ID" to transactionResult.authID,
                                                    "Bank Name" to mBankName,
                                                    "Beneficiary" to mAccountName,
                                                    "Fee" to (mConvenienceFee.toInt()/100).toString()
                                            )

                                            val date = TimeUtils.convertLongToString(transactionResult.longDateTime)
                                            val receiptModel = ReceiptModel(date, "Transfer", transactionResult.transactionStatus, map,  (transactionResult.amount.toInt() / 100).toString(), transactionResult.transactionStatusReason)

                                            val intent = Intent(this@TransferAmountEntry, PrintActivity::class.java)
                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_MODEL_KEY, receiptModel)
                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.NOT_INCLUDED)
                                            //finish()
                                            startActivity(intent)
                                            finish()
                                        }

                                    }
                                }
                            }.show()

                        }

                        DeviceState.APPROVED -> { completeCardPayment()
                            alert {
                                title = "Transaction Result"
                                message = "Transaction approved."
                                positiveButton(buttonText = "Print"){
                                    (application as App).db.transactionResultDao.get(mRrn).observe({lifecycle}){

                                        it?.let {
                                            transactionResult ->

                                            val map = hashMapOf<String, String>(
                                                    "MID" to transactionResult.merchantID,
                                                    "RRN" to transactionResult.RRN,
                                                    "Transaction approved" to "True",
                                                    "Terminal ID" to mTerminalId,
                                                    "Card Holder" to transactionResult.cardHolderName,
                                                    "Card Expiry" to transactionResult.cardExpiry,
                                                    "PAN" to transactionResult.PAN,
                                                    "STAN" to transactionResult.STAN,
                                                    "Auth ID" to transactionResult.authID,
                                                    "Bank Name" to mBankName,
                                                    "Beneficiary" to mAccountName,
                                                    "Fee" to (mConvenienceFee.toInt()/100).toString()
                                            )

                                            val date = TimeUtils.convertLongToString(transactionResult.longDateTime)
                                            val receiptModel = ReceiptModel(date, "Transfer", transactionResult.transactionStatus, map,  (transactionResult.amount.toInt() / 100).toString(), transactionResult.transactionStatusReason)

                                            val intent = Intent(this@TransferAmountEntry, PrintActivity::class.java)
                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_MODEL_KEY, receiptModel)
                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.NOT_INCLUDED)
                                            startActivity(intent)
                                            finish()
                                        }

                                    }
                                }
                            }.show()
                        } else -> {
                        toast("No data!")
                    }
                    }
            }
        }
    }

    private fun verifyAccountDetails(){
        val progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        progressDialog.setTitle("Verification")
        progressDialog.setMessage("Now looking for account details")
        progressDialog.show()

        val accountDetails = AccountLookUpDetails(action = "lookup", beneficiary = mAccountNumber, vendorBankCode =mBankCode, walletID = mWalletId, username = mWalletUsername, password = mWalletPassword, amount = (txtAmount.text.toString().toDouble() * 100).toString())

        launch(CommonPool){
            try {
                val response = TransferService.create().lookUpAccountNumber(accountDetails).await()
                val amount = txtAmount.text.toString()
                try {
                    launch(UI){
                        progressDialog.dismiss()
                        if (response.status != 1){
                            alert {
                                title = "Response"
                                message = "${response.message}"
                                okButton { }
                            }.show()
                        } else {
                            mAccountName = response.beneficiaryName
                            mConvenienceFee = response.convenienceFee.toString()
                            alert {
                                title = "Account number"
                                message = "${response.message} - ${response.beneficiaryName}\nAmount - N$amount\nConvenience fee - N${response.convenienceFee.toFloat()/100}"
                                okButton { paymentSelection(response) }
                            }.show()
                        }

                    }

                } catch (e : Exception){
                    launch(UI){
                        progressDialog.dismiss()
                        alert {
                            title = "Response"
                            message = response.message
                        }.show()
                    }
                }
            }
            catch (e : SocketTimeoutException){
                launch(UI){
                    progressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Connection taking too long to be established. Please try again"
                        okButton { onBackPressed() }
                    }.show()
                }

            }
            catch (e : ConnectException){
                launch(UI){
                    progressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Connection not established. Please try again"
                        okButton {  }
                    }.show()
                }

            }
            catch (e : retrofit2.HttpException){
                launch(UI){
                    progressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton {  }
                    }.show()
                }
            }

        }

    }

    private fun paymentSelection(response: LookupSuccessModel) {
        alert {
            title = "Transaction Type"
            message = "Select the type of transaction you want to make"
            positiveButton(buttonText = "Card") { _ -> payWithCard(response) }
            negativeButton(buttonText = "Wallet") { _ -> payWithWallet(response) }
        }.show()
    }


}
