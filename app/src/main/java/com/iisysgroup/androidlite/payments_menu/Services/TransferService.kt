package com.iisysgroup.androidlite.payments_menu.Services

import com.google.gson.GsonBuilder
import com.iisysgroup.androidlite.models.LookupSuccessModel
import com.iisysgroup.androidlite.payments_menu.models.AccountLookUpDetails
import com.iisysgroup.androidlite.payments_menu.models.TransactionDetails
import com.iisysgroup.androidlite.payments_menu.models.TransactionResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import kotlinx.coroutines.experimental.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface TransferService {

    //API endpoint for account number lookup
    @POST("/tams/tams/transfer-engine.php")
    fun lookUpAccountNumber(@Body lookUpRequestDetails :  AccountLookUpDetails) : Deferred<LookupSuccessModel>

    //API endpoint for transfer
    @POST("/tams/tams/transfer-engine.php")
    fun transfer(@Body transferModel : TransactionDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<TransactionResponse>

    //API endpoint for withdraw
    @POST("/tams/tams/transfer-engine.php")
    fun withdraw(@Body transferModel : TransactionDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<TransactionResponse>

    //API endpoint for deposit - cash
    @POST("/tams/tams/transfer-engine.php")
    fun deposit(@Body transferModel : TransactionDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<TransactionResponse>




    companion object Factory {
        private val BASE_URL = "http://197.253.19.75"
        fun create(): TransferService {
            val clientBuilder = OkHttpClient.Builder()

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            clientBuilder.connectTimeout(20, TimeUnit.SECONDS)
            clientBuilder.readTimeout(30, TimeUnit.SECONDS)
            clientBuilder.writeTimeout(30, TimeUnit.SECONDS)

            clientBuilder.addInterceptor(logging)


            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder().client(clientBuilder.build()).addConverterFactory(GsonConverterFactory.create(gson)).addCallAdapterFactory(CoroutineCallAdapterFactory()).baseUrl(BASE_URL).build()
            val service = retrofit.create(TransferService::class.java)

            return service
        }
    }
}