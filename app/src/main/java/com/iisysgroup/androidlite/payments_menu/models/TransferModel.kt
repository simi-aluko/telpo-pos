package com.iisysgroup.androidlite.payments_menu.models

import com.google.gson.annotations.Expose
import com.iisysgroup.androidlite.payments_menu.transfer.TransferAmountEntry

data class AccountLookUpDetails(@Expose val action : String, @Expose val beneficiary : String, @Expose val vendorBankCode : String, @Expose val walletID : String, @Expose val username : String, @Expose val password : String, @Expose val amount: String)

data class TransactionDetails(@Expose val action : String, @Expose val method : String, @Expose val amount : String, @Expose val beneficiary: String, @Expose val vendorBankCode: String, @Expose val walletID: String, @Expose val username: String, @Expose val password: String, @Expose val pin : String)

data class TransactionResponse(@Expose val status : Int, @Expose val message : String)

data class TransferDetails(val transactionType : TransferAmountEntry.TRANSACTION_TYPE, val isApproved : Boolean, val beneficiary : String, val amount : String, val fee : String, val bankName : String, val terminalId : String)