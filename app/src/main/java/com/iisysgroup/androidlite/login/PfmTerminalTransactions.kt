package com.iisysgroup.payvice.models

/**
 * Created by simileoluwaaluko on 20/05/2018.
 */
data class PfmTerminalTransactions(
        var term_id: String,
        var terminalTransactionSum: Double,
        var terminalTransactionCount: Int

)