package com.iisysgroup.payvice.base.presenter

interface LoginPresenter {

    fun login(userID: String, password: String)
}