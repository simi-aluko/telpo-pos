package com.iisysgroup.androidlite.db

import com.iisysgroup.poslib.host.entities.VasTerminalData
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface VasTerminalService {

    @GET("xmerchant.php")
    fun getVasTerminalDetails() : Single<VasTerminalData>


    object Factory {
        private val baseUrl = "http://197.253.19.75/tams/eftpos/op/"
        fun getService() : VasTerminalService {
            val retrofitBuilder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(baseUrl)
                    .build()

            return retrofitBuilder.create(VasTerminalService::class.java)
        }
    }
}