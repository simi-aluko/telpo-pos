package com.iisysgroup.androidlite.vas.cable

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.iisysgroup.poslib.host.entities.TransactionResult

data class DstvLookup(@Expose val unit : String = "dstv", @Expose val iuc : String)

data class GotvLookupModel(@Expose val unit : String = "gotv", @Expose val iuc : String)

data class DstvResponse(@Expose val fullname : String?, @Expose val unit : String?, @Expose val data : List<Data>)


data class Data(@Expose val name : String, @Expose val product_code : String, @Expose val amount : String)

data class PayDetails(@Expose val iuc: String, @Expose val product_code: String, @Expose val user_id : String, @Expose val terminal_id : String, @Expose val pin: String, @Expose val reference : String = "", @Expose val unit : String, @Expose var lat : String, @Expose var long : String)

data class PayResponse(@Expose val error : Boolean, @Expose val message : String, @Expose val ref : String, @Expose val date : String)

@Entity
data class DstvBeneficiariesModel(@PrimaryKey val dstvNumber : String, val name : String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dstvNumber)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DstvBeneficiariesModel> {
        override fun createFromParcel(parcel: Parcel): DstvBeneficiariesModel {
            return DstvBeneficiariesModel(parcel)
        }

        override fun newArray(size: Int): Array<DstvBeneficiariesModel?> {
            return arrayOfNulls(size)
        }
    }
}
