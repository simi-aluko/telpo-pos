package com.iisysgroup.androidlite.vas.activity.Genesis.adapters

import android.content.Context
import android.preference.PreferenceManager
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.itex.richard.payviceconnect.model.Genesis
import com.itex.richard.payviceconnect.wrapper.PayviceServices
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog

class MovieListAdapter(internal var mData: List<Genesis.Movies>, internal var context: Context, internal var onMovieSelectedListener: OnMovieSelectedListener) : RecyclerView.Adapter<MovieListAdapter.MyViewHolder>() {
   public interface OnMovieSelectedListener {
        fun startWalletPayment(movies : Genesis.Movies)
        fun startCardPayment(movies: Genesis.Movies)
    }

    internal var layoutInflater: LayoutInflater

    init {
        this.layoutInflater = LayoutInflater.from(context)
    }

    private val mProgressDialog by lazy {
       context.indeterminateProgressDialog("Processing")
    }
    private val mPayvicePin by lazy {
        PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.key_payvice_wallet_pin), "")
    }

    private val mWalletUsername by lazy {
        SharedPreferenceUtils.getPayviceUsername(context)
    }

    private val mWalletId by lazy {
        SharedPreferenceUtils.getPayviceWalletId(context)
    }

    private val mWalletPassword by lazy {
        SharedPreferenceUtils.getPayvicePassword(context)
    }

    private val payviceServices by lazy {
        PayviceServices.getInstance(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListAdapter.MyViewHolder {
        val view = layoutInflater.inflate(R.layout.movie_view, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieListAdapter.MyViewHolder, position: Int) {
        val movie = mData[position]
        holder.setData(movie)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var movieImage: ImageView
        internal var name: TextView
        internal var price: TextView
        internal var time: TextView
        internal var movieView: CardView

        init {
            movieImage = itemView.findViewById(R.id.movieimg)
            name = itemView.findViewById(R.id.name)
            price = itemView.findViewById(R.id.moviePrice)
            time = itemView.findViewById(R.id.movieTime)
            movieView = itemView.findViewById(R.id.cardview)
        }

        fun setData(movie: Genesis.Movies) {
            Glide.with(context)
                    .load(movie.poster)
                    .into(movieImage)

            name.text = movie.short_title
            price.text = movie.amount
            time.text = movie.start_date + " " + movie.start_date
            movieView.setOnClickListener {
                context.alert {
                    title = "Transaction Type"
                    message = "Select the type of transaction you want to make"
                    positiveButton(buttonText = "Card") { _ -> onMovieSelectedListener.startCardPayment(movie)}
                    negativeButton(buttonText = "Wallet") {_ -> onMovieSelectedListener.startWalletPayment(movie)}
                }.show()
            }
        }
    }



}
