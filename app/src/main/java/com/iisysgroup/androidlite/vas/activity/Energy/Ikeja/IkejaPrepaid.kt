package com.iisysgroup.androidlite.vas.activity.energy.Ikeja

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.PrintActivity
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.TermMagmActivity
import com.iisysgroup.androidlite.cardpaymentprocessors.PurchaseProcessor
import com.iisysgroup.androidlite.cardpaymentprocessors.VasPurchaseProcessor
import com.iisysgroup.androidlite.login.Helper
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.models.ReceiptModel
import com.iisysgroup.androidlite.payments_menu.BasePaymentActivity
import com.iisysgroup.androidlite.utils.PinAlertUtils
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.androidlite.vas.services.IkejaService
import com.iisysgroup.payvice.securestorage.SecureStorageUtils
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.utils.AccountType
import kotlinx.android.synthetic.main.activity_ikeja_prepaid.*
import kotlinx.android.synthetic.main.content_prepaid.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast
import java.net.ConnectException
import java.net.SocketTimeoutException

@Suppress("IMPLICIT_CAST_TO_ANY")
class IkejaPrepaid : AppCompatActivity() {

    private var isCard = false

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }

    private lateinit var mEncryptedPin : String

    private val wallet_username by lazy {
        SharedPreferenceUtils.getPayviceUsername(this)
    }

    private val wallet_id by lazy {
        SharedPreferenceUtils.getPayviceWalletId(this)
    }

    private val wallet_password by lazy {
        SecureStorage.retrieve(Helper.PLAIN_PASSWORD, "")
    }

    private val mProgressDialog by lazy {
        indeterminateProgressDialog("Processing")
    }

    lateinit var meterNumber : String
    lateinit var amount : String
    lateinit var phoneNumber : String


    private lateinit var mIkejaPrepaidLookupSuccess : IkejaModel.IkejaLookUpSuccessResponse


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ikeja_prepaid)

        setSupportActionBar(toolbar)

        submit.setOnClickListener { showPhoneNumberInput() }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    private fun showPhoneNumberInput() {
        val customerNumber = customerNumber.text.toString()

        if (customerNumber.length != 11){
            this.customerNumber.error = "Enter valid customer number"
            return
        }

        MaterialDialog.Builder(this@IkejaPrepaid).title("Phone number input").content("Please enter your phone number").inputType(InputType.TYPE_CLASS_PHONE).input("Phone number", "") { _, input -> handleIkejaPayments(input.toString(), customerNumber) }.show()
    }

    private fun handleIkejaPayments(phoneNumber : String, customerNumber: String) {
        mProgressDialog.show()

        try {
            async {
                val details = IkejaModel.IkejaLookupDetails(meter = customerNumber, account = "", user_id = wallet_username, password = wallet_password, terminal_id = wallet_id)
                val response = IkejaService.create().ikejaLookup(details).await()

                val jsonResponse = Gson().toJsonTree(response)

                val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()

                launch(UI){
                    mProgressDialog.hide()
                }

                if (jsonResponse.toString().contains("\"error\":true"))
                {
                    launch(UI){
                        toast("Error")
                        val response = gson.fromJson(jsonResponse, IkejaModel.IkejaLookUpFailedResponse::class.java)
                        alert {
                            title = "Validation error"
                            message = response.message
                        }.show()
                    }
                }
                else
                {
                    mIkejaPrepaidLookupSuccess = gson.fromJson(jsonResponse, IkejaModel.IkejaLookUpSuccessResponse::class.java)
                    launch(UI){
                        alert {
                            title = "Confirm"
                            message = "Name : ${mIkejaPrepaidLookupSuccess.name}\nAddress : ${mIkejaPrepaidLookupSuccess.address}\nAgent : ${mIkejaPrepaidLookupSuccess.agent}"
                            okButton { enterAmount(customerNumber, phoneNumber) }
                        }.show()
                    }
                }
            }
        } catch (error: ConnectException) {
            mProgressDialog.dismiss()
            toast("Connection error, Check your internet connection")
        } catch (error: SocketTimeoutException) {
            mProgressDialog.dismiss()
            toast("Connection taking too long. Please try again")
        } catch (e: retrofit2.HttpException) {
            launch(UI) {
                mProgressDialog.dismiss()
                alert {
                    title = "Error"
                    message = "Error from server. Please try again"
                    okButton { }
                }.show()
            }
        }

    }

    private fun enterAmount(meter : String, phoneNumber: String) {
        MaterialDialog.Builder(this@IkejaPrepaid).title("Enter amount").content("Amount").inputType(InputType.TYPE_CLASS_NUMBER).input("Amount", "") { _, input -> selectTransactionType(meter, phoneNumber, input.toString()) }.show()
    }

    private fun selectTransactionType(meter: String, phoneNumber: String, amount : String){
        alert {
            title = "Transaction Type"
            message = "Select the type of transaction you want to make"
            positiveButton(buttonText = "Card") { _ -> payWithCard(meter, phoneNumber, amount)}
            negativeButton(buttonText = "Wallet") {_ -> payWithWallet(meter, phoneNumber, amount)}
        }.show()
    }

    private fun payWithCard(meter : String, phoneNumber: String, amount: String){

        val view = View.inflate(this, R.layout.activity_enter_pin, null)
        val encryptedPassword = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")


        PinAlertUtils.getPin(this, view){
            mEncryptedPin = SecureStorageUtils.hashIt(it!!, encryptedPassword)!!


            isCard = true

            meterNumber = meter
            this.phoneNumber = phoneNumber
            this.amount = amount
            val intent = Intent(this, VasPurchaseProcessor::class.java)
            intent.putExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE, AccountType.DEFAULT_UNSPECIFIED)

            //amount * 100 to convert the amount to long
            intent.putExtra(BasePaymentActivity.TRANSACTION_AMOUNT,  (amount.toLong() * 100))
            intent.putExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)

            if (SharedPreferenceUtils.getIsTerminalPrepped(this)){
                startActivityForResult(intent, IkejaPrepaid.KEYS.IKEJA_PREPAID_INTENT_CODE)
            } else {
                alert {
                    isCancelable = false
                    title = "Terminal not configured"
                    message = "Click O.K to go to configuration page"
                    okButton {
                        startActivity(Intent(this@IkejaPrepaid, TermMagmActivity::class.java))
                    }
                }.show()
            }
        }

    }

    private fun payWithWallet(customerNumber: String, phoneNumber: String, amount: String){
        isCard = false
        mProgressDialog.show()

        val view = View.inflate(this, R.layout.activity_enter_pin, null)
        val encryptedPassword = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")

        PinAlertUtils.getPin(this, view){
            val encryptedPin = SecureStorageUtils.hashIt(it!!, encryptedPassword)

            try {
                async {
                    val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
                    val payDetails = IkejaModel.IkejaPayDetails(meter = customerNumber, account = "", amount = amount, phone = phoneNumber, service_type = "vend", password = wallet_password, terminal_id = wallet_id, user_id = wallet_username, pin = encryptedPin!!,lat = SharedPreferenceUtils.getLatitude(this@IkejaPrepaid), long = SharedPreferenceUtils.getLongitude(this@IkejaPrepaid))



                    val request = IkejaService.create().pay(payDetails).await()
                    val jsonResponse = Gson().toJsonTree(request).asJsonObject



                    launch(UI){
                        mProgressDialog.dismiss()
                    }
                    if (jsonResponse.toString().contains("\"error\":true")){
                        val response = gson.fromJson(jsonResponse.toString(), IkejaModel.IkejaPayFailedResponse::class.java)


                        launch(UI) {
                            alert {
                                title = "Response"
                                message = "Error : ${response.message}"
                                okButton { moveToHome() }
                            }.show()
                        }

                    } else {
                        val response = gson.fromJson(jsonResponse.toString(), IkejaModel.IkejaPaySuccessResponse::class.java)
                        launch(UI){
                            alert {
                                title = "Response"
                                message = "Payer : ${response.payer}\nToken : ${response.token}\nAccount Type : ${response.account_type}\nClient ID : ${response.client_id}\nSGC : ${response.sgc}\nMSNO : ${response.msno}\nKRN : ${response.krn}\nTI : ${response.ti}\nTT : ${response.tt}\nUnit Value : ${response.unit_value}\nUnit Cost : ${response.unit_cost}\nAgent : ${response.agent}\nVAT : ${response.vat}\nAddress : ${response.address}"


                                positiveButton(buttonText = "Print"){
                                    val receiptMap = hashMapOf<String, String>(
                                            "Terminal ID" to SharedPreferenceUtils.getTerminalId(this@IkejaPrepaid),
                                            "Token" to response.token,
                                            "Wallet ID" to wallet_id,
                                            "Payer" to response.payer,
                                            "Account Type" to response.account_type,
                                            "Client ID" to response.client_id,
                                            "SGC" to response.sgc,
                                            "MSNO" to response.msno,
                                            "KRN" to response.krn,
                                            "TI" to response.ti,
                                            "Agent" to response.agent,
                                            "VAT" to response.vat,
                                            "Address" to response.address
                                    )

                                    val receiptModel = ReceiptModel(response.date, "IKEJA PREPAID PURCHASE", "APPROVED", receiptMap, response.amount, "")

                                    val intent = Intent(this@IkejaPrepaid, PrintActivity::class.java)
                                    intent.putExtra("print_map", receiptModel)
                                    intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.IKEDC_PREPAID)
                                    startActivity(intent)
                                    finish()
                                }
                            }.show()
                        }
                    }
                }
            } catch (error: ConnectException) {
                mProgressDialog.dismiss()
                toast("Connection error, Check your internet connection")
            } catch (error: SocketTimeoutException) {
                mProgressDialog.dismiss()
                toast("Connection taking too long. Please try again")
            } catch (e: retrofit2.HttpException) {
                launch(UI) {
                    mProgressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton { }
                    }.show()
                }
            }

        }

    }



    private fun moveToHome() {
        finish()
        val intent = Intent(this, IkejaElectric::class.java)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            IkejaPrepaid.KEYS.IKEJA_PREPAID_INTENT_CODE -> when (resultCode){
                Activity.RESULT_OK -> {
                    val state = data?.getSerializableExtra("state") as DeviceState
                    val rrn = data.getStringExtra("rrn")


                    when (state){
                        DeviceState.APPROVED -> {
                            (application as App).db.transactionResultDao.get(rrn).observe({lifecycle}){ transactionResult ->
                                transactionResult?.let { transactionResult ->

                                        mProgressDialog.show()
                                        launch(CommonPool){
                                            val payDetails = IkejaModel.IkejaPayDetails(account = "", meter = meterNumber, service_type = "vend", amount = amount, phone = phoneNumber, terminal_id = wallet_id, user_id = wallet_username, password = wallet_password, pin = mEncryptedPin, lat = SharedPreferenceUtils.getLatitude(this@IkejaPrepaid), long = SharedPreferenceUtils.getLongitude(this@IkejaPrepaid))
                                            val cardResponse = IkejaService.Factory.create().payWithCard(payDetails).await()

                                            val jsonResponse = Gson().toJsonTree(cardResponse).asJsonObject
                                            val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
                                            if (cardResponse.toString().contains("error:true")){
                                                val response = gson.fromJson(jsonResponse.toString(), IkejaModel.IkejaPayFailedResponse::class.java)
                                                launch(UI){
                                                    alert {
                                                        title = "Response"
                                                        message = "Error : ${response.message}"
                                                        positiveButton(buttonText = "Print"){
                                                            val receiptMap = hashMapOf<String, String>(
                                                                    "Terminal ID" to SharedPreferenceUtils.getTerminalId(this@IkejaPrepaid),
                                                                    "Wallet ID" to wallet_id,
                                                                    "RRN" to transactionResult.RRN,
                                                                    "Card PAN" to transactionResult.PAN,
                                                                    "CardHolder" to transactionResult.cardHolderName,
                                                                    "Card Expiry" to transactionResult.cardExpiry,
                                                                    "Auth ID" to transactionResult.authID,
                                                                    "MID" to transactionResult.merchantID,
                                                                    "STAN" to transactionResult.STAN,
                                                                    "Payer" to mIkejaPrepaidLookupSuccess.name,
                                                                    "Account Type" to "IKEJA PREPAID",
                                                                    "Agent" to mIkejaPrepaidLookupSuccess.agent,
                                                                    "Address" to mIkejaPrepaidLookupSuccess.address
                                                            )

                                                            val receiptModel = ReceiptModel(response.date, "IKEJA PREPAID PURCHASE", transactionResult.transactionStatus, receiptMap, (transactionResult.amount/100).toString(), transactionResult.transactionStatusReason)

                                                            val intent = Intent(this@IkejaPrepaid, PrintActivity::class.java)
                                                            intent.putExtra("print_map", receiptModel)
                                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.IKEDC_PREPAID)
                                                            startActivity(intent)
                                                            finish()
                                                        }
                                                    }.show()
                                                }

                                            } else {
                                                val response = gson.fromJson(jsonResponse.toString(), IkejaModel.IkejaPaySuccessResponse::class.java)
                                                launch(UI){
                                                    alert {
                                                        title = "Response"
                                                        message = "Payer : ${response.payer}\nAccount Type : ${response.account_type}\nClient ID : ${response.client_id}\nSGC : ${response.sgc}\nMSNO : ${response.msno}\nKRN : ${response.krn}\nTI : ${response.ti}\nTT : ${response.tt}\nUnit Value : ${response.unit_value}\nUnit Cost : ${response.unit_cost}\nAgent : ${response.agent}\nVAT : ${response.vat}\nAddress : ${response.address}"
                                                        positiveButton(buttonText = "Print"){
                                                            val receiptMap = hashMapOf<String, String>(
                                                                    "Terminal ID" to SharedPreferenceUtils.getTerminalId(this@IkejaPrepaid),
                                                                    "Wallet ID" to wallet_id,
                                                                    "RRN" to transactionResult.RRN,
                                                                    "Card PAN" to transactionResult.PAN,
                                                                    "CardHolder" to transactionResult.cardHolderName,
                                                                    "Card Expiry" to transactionResult.cardExpiry,
                                                                    "Auth ID" to transactionResult.authID,
                                                                    "MID" to transactionResult.merchantID,
                                                                    "TOKEN" to response.token,
                                                                    "STAN" to transactionResult.STAN,
                                                                    "Payer" to response.payer,
                                                                    "Account Type" to response.account_type,
                                                                    "Client ID" to response.client_id,
                                                                    "SGC" to response.sgc,
                                                                    "MSNO" to response.msno,
                                                                    "KRN" to response.krn,
                                                                    "TI" to response.ti,
                                                                    "Agent" to response.agent,
                                                                    "VAT" to response.vat,
                                                                    "Token" to response.token,
                                                                    "Address" to response.address
                                                            )

                                                            val receiptModel = ReceiptModel(response.date, "IKEJA PREPAID PURCHASE", transactionResult.transactionStatus, receiptMap, response.amount, transactionResult.transactionStatusReason)

                                                            val intent = Intent(this@IkejaPrepaid, PrintActivity::class.java)
                                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.IKEDC_PREPAID)
                                                            intent.putExtra("print_map", receiptModel)
                                                            intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.IKEDC_PREPAID)
                                                            startActivity(intent)
                                                            finish()
                                                        }

                                                    }.show()
                                                }
                                            }
                                        }
                                    }
                                }
                        }

                        DeviceState.DECLINED -> {
                            alert {
                                title = "Card Processing Response"
                                message = "Transaction was declined"
                                okButton {  }
                            }.show()
                        }

                        DeviceState.FAILED -> {
                            alert {
                                title = "Card Processing Response"
                                message = "Transaction was declined"
                                okButton {  }
                            }.show()
                        }
                    }
                }
            }
        }
    }

    object KEYS {
        const val IKEJA_PREPAID_INTENT_CODE = 23922
    }
}
