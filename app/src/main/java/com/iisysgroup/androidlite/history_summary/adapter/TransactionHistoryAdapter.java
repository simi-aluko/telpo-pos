package com.iisysgroup.androidlite.history_summary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iisysgroup.androidlite.R;
import com.iisysgroup.androidlite.history_summary.model.TransactionHistoryModel;

import java.util.List;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.MyVViewHolder> {
    private List<TransactionHistoryModel> mData;
    private LayoutInflater inflater;
    Context cont;

    public TransactionHistoryAdapter(Context context, List<TransactionHistoryModel> historyModelList){
        this.cont = context;
        this.mData = historyModelList;
        this.inflater = LayoutInflater.from(context);
        Log.i("jjj", mData.size() + "");
    }

    @NonNull
    @Override
    public TransactionHistoryAdapter.MyVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.history_list_view,parent,false);
        MyVViewHolder myVViewHolder = new MyVViewHolder(view);
        return myVViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionHistoryAdapter.MyVViewHolder holder, int position) {
            holder.setData(mData.get(position), position);
    }

    @Override
    public int getItemCount() {
        Log.i("okh2", mData.size() + "");
        return mData.size();
    }

    public class MyVViewHolder extends RecyclerView.ViewHolder{
        TextView productName, date, amount,status;
        LinearLayout cardView;

        public MyVViewHolder(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.txtProductName);
            date = itemView.findViewById(R.id.date);
            amount = itemView.findViewById(R.id.amount);
            status = itemView.findViewById(R.id.status);
            cardView = itemView.findViewById(R.id.cardview);
        }

        public void setData(TransactionHistoryModel transactionHistoryModel, int position) {
            productName.setText(transactionHistoryModel.getProduct());
            date.setText(transactionHistoryModel.getDate());
            if(transactionHistoryModel.getType().equals("Debit")){
                amount.setTextColor(cont.getResources().getColor(R.color.colorAccent));
            }else{
                amount.setTextColor(cont.getResources().getColor(R.color.colorGreen));
            }
            amount.setText(transactionHistoryModel.getAmount());
            if(transactionHistoryModel.getStatus()){
                status.setTextColor(cont.getResources().getColor(R.color.colorGreen));
                status.setText("Approved");
            }else{
                status.setTextColor(cont.getResources().getColor(R.color.colorAccent));
                status.setText("Declined");
            }
        }
    }
}
