package com.iisysgroup.androidlite.history_summary

import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.history_summary.model.BalanceModel
import com.iisysgroup.androidlite.login.Helper
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_wallet_balance.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import okhttp3.MediaType
import org.jetbrains.anko.indeterminateProgressDialog
import okhttp3.RequestBody
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton


class WalletBalance : AppCompatActivity() {
    val progress by lazy {
        indeterminateProgressDialog(
                message = "Please wait",
                title = "Loading"
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet_balance)
        progress.setCancelable(false)
        progress.show()
        if(isInternetAvailable()){
            Single.fromCallable {
                getBalance(SharedPreferenceUtils.getPayviceUsername(this@WalletBalance), SecureStorage.retrieve(Helper.PLAIN_PASSWORD, ""))
            }.subscribeOn(Schedulers.io()).subscribe(Consumer {
                //Do nothing
            }, Consumer {
                alert {
                    title = "Error"
                    message = "Error occured. Try Again"
                    positiveButton(buttonText = "Ok", onClicked = {
                        this@WalletBalance.finish()
                    })
                }.show()
            })
        }else{
            alert {
                title = "Error"
                message = "No internet connection"
                positiveButton(buttonText = "Ok", onClicked = {
                    this@WalletBalance.finish()
                })
            }.show()
        }


    }

    fun isInternetAvailable(): Boolean{
        val cm = this@WalletBalance.getSystemService(android.content.Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activityNetworkInfo : NetworkInfo? = null
        activityNetworkInfo = cm.activeNetworkInfo
        return activityNetworkInfo != null &&activityNetworkInfo.isConnected
    }


    fun getBalance(email : String, password : String) {
        val client = OkHttpClient()

        val mediaType = MediaType.parse("application/json")
        val body = RequestBody.create(mediaType, "{\"username\": \""+email+"\",\"password\": \""+password+"\"}")
        val request = Request.Builder()
                .url("https://www.payvice.com/api/account")
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build()

        val response = client.newCall(request).execute()
        if(response.isSuccessful){
            progress.dismiss()
            val balanceModel = Gson().fromJson(response.body()!!.string(), BalanceModel::class.java)
            launch(UI){
                if(balanceModel.error){
                    alert {
                        title = "Error"
                        message = balanceModel.message
                        positiveButton(buttonText = "Close", onClicked = {
                            this@WalletBalance.finish()
                        })
                    }.show()
                }
                else{
                    Log.i("okh", balanceModel.toString())
                    Log.i("okh", balanceModel.balance.toString() + " " + balanceModel.commissionBalance.toString())
                    balance.text = "NGN " + balanceModel.balance.toString()
                    commision.text = "NGN " + balanceModel.commissionBalance.toString()
                    emailTxt.text = balanceModel.email
                    name.text = balanceModel.name
                    walletId.text = balanceModel.walletID

                }
            }
        }else{
            progress.dismiss()
            launch(UI){
                alert {
                    title = "Error"
                    message = "Unknown error occured." + response.code()
                    okButton {
                        this@WalletBalance.finish()
                    }
                }.show()
            }
        }
    }
}
