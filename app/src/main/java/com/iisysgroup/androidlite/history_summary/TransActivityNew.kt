package com.iisysgroup.androidlite.history_summary

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.history_summary.adapter.TransactionHistoryAdapter
import com.iisysgroup.androidlite.history_summary.model.TrackHistoryModel
import com.iisysgroup.androidlite.history_summary.service.NewHistoryService
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_trans_new.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog

class TransActivityNew : AppCompatActivity() {
    val progress by lazy {
        indeterminateProgressDialog(
                message = "Please Wait",
                title = "Fetching"
        )
    }

   lateinit var histpryAdapter: TransactionHistoryAdapter
   lateinit var trackHistoryModel: TrackHistoryModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trans_new)
        setSupportActionBar(toolbar)
        progress.setCancelable(false)
        progress.show()

        Single.fromCallable<TrackHistoryModel> {
            NewHistoryService(this@TransActivityNew).firstPage
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(Consumer {
                    progress.dismiss()
                    if(it.isError){
                        alert{
                            message = it.message
                            title = "Error"
                            positiveButton( buttonText = "OK", onClicked = {
                                this@TransActivityNew.finish()
                                it.dismiss()
                            })
                        }.show()
                    }else{
                        trackHistoryModel = it
                        if(it.transactionHistoryModelList != null){
                            if(it.transactionHistoryModelList.size <= 0){
                                alert{
                                    message = "No transaction records from server,please try again"
                                    title = "Error"
                                    positiveButton( buttonText = "OK", onClicked = {
                                        this@TransActivityNew.finish()
                                        it.dismiss()
                                    })
                                }.show()
                            }
                        }
                        setUpRecyclerView(it)
                    }


                }, Consumer {
                    progress.dismiss()
                    alert{
                        message = "Error occured, try again"
                        title = "Error"
                        positiveButton( buttonText = "OK", onClicked = {
                            this@TransActivityNew.finish()
                            it.dismiss()
                        })
                    }.show()
                })






    }

    private fun setUpRecyclerView(trackHistoryMode : TrackHistoryModel){
        histpryAdapter = TransactionHistoryAdapter(this@TransActivityNew, trackHistoryMode.transactionHistoryModelList)
        var linearLayoutManager = LinearLayoutManager(this@TransActivityNew)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = histpryAdapter


    }
}
