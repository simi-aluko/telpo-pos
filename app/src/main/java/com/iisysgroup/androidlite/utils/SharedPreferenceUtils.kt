package com.iisysgroup.androidlite.utils

import android.content.Context
import android.preference.PreferenceManager
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.login.Helper
import com.iisysgroup.androidlite.login.securestorage.SecureStorage

object SharedPreferenceUtils {

    fun getSharedPreferences(context : Context) = PreferenceManager.getDefaultSharedPreferences(context)

    fun getTerminalId(context : Context) = getSharedPreferences(context).getString(context.getString(R.string.key_terminal_id), "")

    fun getIpAddress(context : Context) = getSharedPreferences(context).getString(context.getString(R.string.key_ip_address), "")

    fun getPort(context : Context) = getSharedPreferences(context).getString(context.getString(R.string.key_pref_port), "")

    fun isSsl(context: Context) = getSharedPreferences(context).getString(context.getString(R.string.key_pref_port_type), "") == "SSL"

    fun getPayviceWalletId(context : Context) = SecureStorage.retrieve(Helper.TERMINAL_ID, "")

    fun getPayvicePassword(context : Context) = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")

    fun getPayviceUsername(context : Context) = SecureStorage.retrieve(Helper.USER_ID, "")

    fun setUserLoggedIn(context : Context, isLoggedIn: Boolean) = getSharedPreferences(context).edit().putBoolean(context.getString(R.string.key_is_user_logged_in), isLoggedIn).apply()

    fun getUserLoggedIn(context : Context) = getSharedPreferences(context).getBoolean(context.getString(R.string.key_is_user_logged_in), false)

    fun getIsTerminalPrepped(context : Context) = getSharedPreferences(context).getBoolean(context.getString(R.string.key_is_terminal_prepped), false)

    fun setIsTerminalPrepped(context : Context, isTerminalPrepped : Boolean) = getSharedPreferences(context).edit().putBoolean(context.getString(R.string.key_is_terminal_prepped), isTerminalPrepped).apply()

     fun getLatitude(context: Context) = getSharedPreferences(context).getString("lnt", "0000");
    fun getLongitude(context: Context) = getSharedPreferences(context).getString("lng", "0000");


    fun getNotificationUser(context: Context) = getSharedPreferences(context).getString("notification_user", "itex")
    fun saveNotificationUser(context: Context, user : String){
        var editor = getSharedPreferences(context).edit()
        editor.putString("notification_user", user)
        editor.apply()
    }


}