package com.iisysgroup.androidlite.utils

import android.app.Activity
import android.content.Context
import com.iisysgroup.poslib.host.Host
import com.google.android.gms.location.LocationSettingsStatusCodes
import android.content.IntentSender
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.iisysgroup.androidlite.MainActivity
import com.google.android.gms.location.LocationSettingsResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import android.content.DialogInterface
import android.support.v4.content.ContextCompat.startActivity
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.content.Intent
import android.location.LocationManager
import android.content.Context.LOCATION_SERVICE
import android.provider.Settings
import android.support.v7.app.AlertDialog
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.cardpaymentprocessors.PfmNotification
import com.iisysgroup.poslib.host.entities.TransactionResult


/**
 * Created by Bamitale@Itex on 16/03/2018.
 */

    val Host.TransactionType.stringValue: String
    get() {
        return this.name.replace("_"," ")
    }



        fun AppCompatActivity.sendNotify(rrn: String){
            (application as App).db.transactionResultDao.get(rrn).observe({lifecycle}){ result ->
                result.let{
                    var pfmNotification = PfmNotification()
                    pfmNotification.sendNotification(it!!, this)
                }
            }
        }

        fun AppCompatActivity.sendNotify(rrn: TransactionResult){
            var pfmNotification = PfmNotification()
            pfmNotification.sendNotification(rrn!!, this)
        }

        fun AppCompatActivity.sendNotify(rrn: String, vasPorduct: String,vasCategory : String){
            (application as App).db.transactionResultDao.get(rrn).observe({lifecycle}){ result ->
                result.let{
                    var pfmNotification = PfmNotification()
                    pfmNotification.sendNotification(transactionResult = it!!, context = this, vasPorduct = vasPorduct, vasCategory = vasCategory)
                }
            }
        }

       fun AppCompatActivity.sendNotify(vasPorduct: String,vasCategory : String, amount : Int , context: Context, rescode : String, mid :String){
           var pfmNotification = PfmNotification()
           pfmNotification.sendNotification(vasPorduct,vasCategory,amount,context, rescode, mid)
       }




        fun AppCompatActivity.OnLocation() {
                val lm = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                var gps_enabled = false
                var network_enabled = false

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                } catch (ex: Exception) {
                }


                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                } catch (ex: Exception) {
                }


                if (!gps_enabled) {
                    // notify user
                    val dialog = AlertDialog.Builder(this)
                    dialog.setMessage("Please enable location Service")
                    dialog.setPositiveButton("O.K", DialogInterface.OnClickListener { paramDialogInterface, paramInt ->
                        // TODO Auto-generated method stub
                        val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        this.startActivity(myIntent)
                    })
                    dialog.setCancelable(false)
                    dialog.show()
                }
            }

